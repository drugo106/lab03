public class Game {
	
	private int score;
	private int framescore;
	private int framelaunch;
	private boolean spare;
	
	public Game(){
		this.score = 0;
		this.framescore = 0;
		this.framelaunch = 0;
		this.spare = false;
	}	
	
	public void roll(int pins){
		this.framelaunch+=1;
		this.framescore+=pins;
		this.score+=pins;
		if(this.framelaunch>2){
			if(this.framescore>=10)
				this.score+=pins;
			this.framelaunch = 1;
			this.framescore = pins;
		}
	}
		
	public int score(){
		return this.score;
	}
}

